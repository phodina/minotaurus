# Minotaurus
![Minotaurus](data/Minotaurus_3D_Top.png)

Adapter board for for a camera stack with illumination board
in order to test the sensor on different platforms using 26 FPC connector

The adapter board supports the 4 lane MIPI interface along with I2C bus for
configuration of the sensor and configuration of the illumination controller.

Additional signals are used for Power On of the camera stack board and for
reseting/putting into bootloader the controller responsible for
driving the illumination LEDs.

## Manufacturing
The MIPI lanes are tuned for 100Ω and add additional 42.42mm length. 

The board stackup is JLC04161H-3313.

## License
This project uses the [CERN Open Hardware Licence Version 2 - Permissive](https://ohwr.org/project/cernohl/-/wikis/Documents/CERN-OHL-version-2).
